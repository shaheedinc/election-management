/*
  Created by
  Shaheed Ahmed Dewan Sagar (200401357)
  Anup Benjamin Gomes (200388845)
  For course CS-872
*/
package firestore;

import model.User;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Interface for Firestore Implementation
 */
public interface FirestoreImpl {
    List<User> getAllUsers() throws ExecutionException, InterruptedException;
    void updateServerIpOnUser(User user, String serverUrl) throws ExecutionException, InterruptedException;
    void startListeningForUser();
}
