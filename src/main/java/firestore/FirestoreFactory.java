/*
  Created by
  Shaheed Ahmed Dewan Sagar (200401357)
  Anup Benjamin Gomes (200388845)
  For course CS-872
*/
package firestore;

// Firestore factory which can return firestore implementation based on project id type
public class FirestoreFactory {

    public static final int FIRESTORE_DEFAULT = 0;
    public static final int FIRESTORE_PROJECT = 1;

    public static FirestoreImpl getFirestore(int type, Object observer)
    {
        if (type == FIRESTORE_DEFAULT) {
            return new FirestoreDefault(observer);
        } else {
            return new FirestoreProject(observer);
        }
    }

}
