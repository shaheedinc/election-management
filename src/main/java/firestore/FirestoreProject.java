/*
  Created by
  Shaheed Ahmed Dewan Sagar (200401357)
  Anup Benjamin Gomes (200388845)
  For course CS-872
*/
package firestore;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.*;
import common.Utils;
import controller.FirestoreHelper;
import model.User;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Observable;
import java.util.concurrent.ExecutionException;

/**
 * Implementation of firestore witha project id specified
 */
public class FirestoreProject implements FirestoreImpl {
    private Firestore db;
    private FirestoreHelper firestoreHelper;

    FirestoreProject(Object observer) {
        try {
            FileInputStream serviceAccount = new FileInputStream("serviceAccountKey.json");
            FirestoreOptions firestoreOptions =
                    FirestoreOptions.getDefaultInstance().toBuilder()
                            .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                            .setProjectId(Utils.FIREBASE_PROJECT_ID)
                            .build();

            this.db = firestoreOptions.getService();
            firestoreHelper = new FirestoreHelper(this.db, observer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<User> getAllUsers() throws ExecutionException, InterruptedException {
        return firestoreHelper.getAllUsers();
    }

    @Override
    public void updateServerIpOnUser(User user, String serverUrl) throws ExecutionException, InterruptedException {
        firestoreHelper.updateServerIpOnUser(user, serverUrl);
    }

    @Override
    public void startListeningForUser() {
        firestoreHelper.startListeningForUser();
    }
}
