/*
  Created by
  Shaheed Ahmed Dewan Sagar (200401357)
  Anup Benjamin Gomes (200388845)
  For course CS-872
*/
package common;

/**
 * Util class to hold static data
 */
public class Utils {

    public static final String FIREBASE_PROJECT_ID = "election-management-91ef9";
    // These IP's will be distributed among the users
    public static final String[] DISTRIBUTED_SERVER_IPS = new String[] {
      "http://142.3.83.4"
    };
    public static final int DISTRIBUTED_SERVER_DEFAULT_PORT = 4567;

    public static String getCompleteServerAddress(String serverIp, int port) {
        return serverIp + ":" + port;
    }

}
