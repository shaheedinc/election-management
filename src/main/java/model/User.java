/*
  Created by
  Shaheed Ahmed Dewan Sagar (200401357)
  Anup Benjamin Gomes (200388845)
  For course CS-872
*/
package model;

/**
 * This is used to map the user collection of firestore
 */
public class User {

    private String id;
    private String name;
    private String dob;
    private String electionId;
    private String serverUrl;

    public User(String id, String name, String dob, String electionId) {
        this.id = id;
        this.name = name;
        this.dob = dob;
        this.electionId = electionId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getElectionId() {
        return electionId;
    }

    public void setElectionId(String electionId) {
        this.electionId = electionId;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }
}
