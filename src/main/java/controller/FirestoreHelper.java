/*
  Created by
  Shaheed Ahmed Dewan Sagar (200401357)
  Anup Benjamin Gomes (200388845)
  For course CS-872
*/
package controller;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.*;
import common.Utils;
import model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ExecutionException;

/**
 * Helper class for firebase related operations.
 */
public class FirestoreHelper extends Observable {

    private Firestore db;

    public FirestoreHelper(Firestore db, Object observer) {
        this.db = db;
        // Adding the observer so that we can fire event to it
        this.addObserver((Observer) observer);
    }

    // getting all users from firestore users collection
    public List<User> getAllUsers() throws ExecutionException, InterruptedException {

        ApiFuture<QuerySnapshot> query = this.db.collection("users").get();

        QuerySnapshot querySnapshot = query.get();
        List<QueryDocumentSnapshot> documents = querySnapshot.getDocuments();
        List<User> allUsers = new ArrayList<>();
        for (QueryDocumentSnapshot document : documents) {
            User user = new User(
                    document.getId(),
                    document.getString("name"),
                    document.getString("dob"),
                    document.getString("electionId"));
            allUsers.add(user);
            System.out.println("User: " + user.getId());
            System.out.println("Name: " + user.getName());
            System.out.println("DOB: " + user.getDob());
            System.out.println("electionID: " + user.getElectionId());
            System.out.println();
        }
        return allUsers;
    }

    // used to update server ip on a specific user in users collections
    public void updateServerIpOnUser(User user, String serverUrl) throws ExecutionException, InterruptedException {
        DocumentReference docRef = db.collection("users").document(user.getId());

        ApiFuture<WriteResult> future = docRef.update("serverUrl", serverUrl);

        future.get();
        System.out.println("User : " + user.getName() + " is set to server : " + serverUrl);
    }

    // used to start listening for new user in users collection
    public void startListeningForUser() {
        System.out.println("Starting to listen");
        this.db.collection("users")
                .addSnapshotListener((snapshots, e) -> {
                    if (e != null) {
                        System.err.println("Listen failed: " + e);
                        return;
                    }

                    for (DocumentChange dc : snapshots.getDocumentChanges()) {
                        if (dc.getType() == DocumentChange.Type.ADDED) {
                            System.out.println("notifying");
                            // setting and notifying the observer about new user
                            setChanged();
                            notifyObservers(dc);
                        }
                    }
                });
    }

}
