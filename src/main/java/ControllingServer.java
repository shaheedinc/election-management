/*
  Created by
  Shaheed Ahmed Dewan Sagar (200401357)
  Anup Benjamin Gomes (200388845)
  For course CS-872
*/

import com.google.cloud.firestore.DocumentChange;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import common.Utils;
import firestore.FirestoreFactory;
import firestore.FirestoreImpl;
import model.User;

import javax.print.Doc;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ExecutionException;

/**
 * This class is used to distribute IP addresses of different distributed server to the user collection
 */
public class ControllingServer implements Observer {

    private FirestoreImpl firestoreImpl;

    private static int serverId = 0;


    private ControllingServer() {
        // Initializing firestore project
        if (Utils.FIREBASE_PROJECT_ID != null && !Utils.FIREBASE_PROJECT_ID.equalsIgnoreCase("")) {
            firestoreImpl = FirestoreFactory.getFirestore(FirestoreFactory.FIRESTORE_PROJECT, this);
        } else {
            firestoreImpl = FirestoreFactory.getFirestore(FirestoreFactory.FIRESTORE_DEFAULT, this);
        }
    }

    private void startListeningForNewUser() {
        // starting to listen for new user in user collection
        firestoreImpl.startListeningForUser();
    }


    public static void main(String[] args) {
        ControllingServer controllingServer = new ControllingServer();
        controllingServer.startListeningForNewUser();
        // Keeping the server alive for new users
        while (true) {}
    }

    // This get called whenever Firestore observable gets a new user
    @Override
    public void update(Observable o, Object arg) {
        DocumentChange dc = (DocumentChange) arg;
        System.out.println("New user: " + dc.getDocument().getData());
        QueryDocumentSnapshot document = dc.getDocument();
        User user = new User(
                document.getId(),
                document.getString("name"),
                document.getString("dob"),
                document.getString("electionId"));
        System.out.println("User: " + user.getId());
        System.out.println("Name: " + user.getName());
        System.out.println("DOB: " + user.getDob());
        System.out.println("electionID: " + user.getElectionId());
        System.out.println();

        try {
            // updating the user collection with it's designated server IP
            firestoreImpl.updateServerIpOnUser(
                    user,
                    Utils.getCompleteServerAddress(
                            Utils.DISTRIBUTED_SERVER_IPS[serverId], Utils.DISTRIBUTED_SERVER_DEFAULT_PORT)
            );
        } catch (ExecutionException | InterruptedException e1) {
            e1.printStackTrace();
        }
        serverId++;
        if (serverId == Utils.DISTRIBUTED_SERVER_IPS.length) {
            serverId = 0;
        }
    }
}
